import 'package:flutter/material.dart';
import 'package:vascomm_flutter/theme/theme.dart';

class ProductCard extends StatefulWidget {
  @override
  _ProductCardState createState() => _ProductCardState();
}

class _ProductCardState extends State<ProductCard> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 176.0,
      width: 160.0,
      decoration: BoxDecoration(
        border: Border.all(
          color: Colors.white,
        ),
        color: Colors.white,
        borderRadius: BorderRadius.circular(16.0),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.1),
            spreadRadius: 2,
            blurRadius: 7,
            offset: Offset(0, 3),
          ),
        ],
      ),
      child: Column(
        children: <Widget>[
          SizedBox(height: 10),
          Stack(
            children: [
              Center(
                child: Image.asset(
                  'assets/images/suntik.png',
                  width: 80.0,
                ),
              ),
              Align(
                alignment: Alignment.topRight,
                child: Container(
                  width: 50.0,
                  height: 30.0,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(36.0),
                    ),
                  ),
                  child: Center(
                    child: Row(
                      children: [
                        Image.asset(
                          'assets/icons/star-ratings.png',
                          width: 22.0,
                          height: 22.0,
                        ),
                        SizedBox(width: 5),
                        Text('5'),
                      ],
                    ),
                  ),
                ),
              )
            ],
          ),
          SizedBox(height: 20),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 8),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Suntik Steril',
                  style: TextStyle(
                    fontFamily: 'Gilroy',
                    fontSize: 14,
                    fontWeight: FontWeight.bold,
                    color: mainColor,
                  ),
                ),
                SizedBox(height: 8),
                Row(
                  children: [
                    Text(
                      'Rp. 10.000',
                      style: TextStyle(
                        fontFamily: 'Gilroy',
                        fontSize: 14,
                        fontWeight: FontWeight.w600,
                        color: Colors.orange,
                      ),
                    ),
                    SizedBox(width: 8),
                    Container(
                      height: 13,
                      color: Color(0xFFB3FFCB),
                      child: Text(
                        'Ready Stock',
                        style: TextStyle(
                          fontFamily: 'Gilroy',
                          fontSize: 10,
                          fontWeight: FontWeight.w600,
                          color: Color(0xFF007025),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
