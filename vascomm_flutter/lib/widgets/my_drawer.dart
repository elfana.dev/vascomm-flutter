import 'package:flutter/material.dart';
import 'package:vascomm_flutter/pages/login_page.dart';
import 'package:vascomm_flutter/pages/profile_page.dart';
import 'package:vascomm_flutter/theme/theme.dart';

class MyDrawer extends StatefulWidget {
  @override
  _MyDrawerState createState() => _MyDrawerState();
}

class _MyDrawerState extends State<MyDrawer> {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(height: 110.0),
          Container(
            child: Row(
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.symmetric(
                    horizontal: 20.0,
                    vertical: 20.0,
                  ),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(30.0),
                    child: Image.asset(
                      'assets/images/profile-picture.png',
                      width: 60,
                    ),
                  ),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    RichText(
                      text: TextSpan(
                        text: 'Angga',
                        style: TextStyle(
                          fontFamily: 'Gilroy',
                          color: mainColor,
                          fontSize: 18,
                          fontWeight: FontWeight.w600,
                        ),
                        children: <TextSpan>[
                          TextSpan(
                            text: ' Praja',
                            style: TextStyle(
                              fontFamily: 'Gilroy',
                              color: mainColor,
                              fontWeight: FontWeight.w200,
                              fontSize: 18,
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 10),
                    Text(
                      'Membership BBLK',
                      style: TextStyle(
                        fontSize: 14,
                        fontFamily: 'Gilroy',
                        color: Color(0xFF002060),
                        fontWeight: FontWeight.w300,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          SizedBox(height: 20),
          InkWell(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => ProfilePage(),
                ),
              );
            },
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: edge),
              child: Row(
                children: [
                  Text(
                    'Profile Saya',
                    style: TextStyle(
                      fontFamily: 'Gilroy',
                      fontSize: 14,
                      fontWeight: FontWeight.w600,
                      color: Colors.grey,
                    ),
                  ),
                  SizedBox(width: 50),
                  IconButton(
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => ProfilePage(),
                        ),
                      );
                    },
                    icon: Icon(
                      Icons.arrow_forward_ios_sharp,
                      size: 18,
                      color: Colors.grey,
                    ),
                  ),
                ],
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: edge),
            child: Row(
              children: [
                Text(
                  'Pengaturan',
                  style: TextStyle(
                    fontFamily: 'Gilroy',
                    fontSize: 14,
                    fontWeight: FontWeight.w600,
                    color: Colors.grey,
                  ),
                ),
                SizedBox(width: 50),
                IconButton(
                  onPressed: () {},
                  icon: Icon(
                    Icons.arrow_forward_ios_sharp,
                    size: 18,
                    color: Colors.grey,
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 40.0),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: edge),
            child: Container(
              height: 35.0,
              width: 145,
              child: RaisedButton(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(23.0),
                ),
                color: Color(0xFFEB0004),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => LoginPage(),
                    ),
                  );
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      'Logout',
                      style: TextStyle(
                        fontFamily: 'Gilroy',
                        fontSize: 14,
                        fontWeight: FontWeight.w400,
                        color: Colors.white,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          SizedBox(height: 86.0),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 50),
            child: Row(
              children: <Widget>[
                Text(
                  'Ikuti kami di ',
                  style: TextStyle(
                    fontFamily: 'Gilroy',
                    fontSize: 18,
                    fontWeight: FontWeight.w500,
                    color: mainColor,
                  ),
                ),
                SizedBox(width: 5.0),
                ImageIcon(
                  AssetImage("assets/icons/facebook-icon.png"),
                  size: 20,
                ),
                SizedBox(width: 10.0),
                ImageIcon(
                  AssetImage("assets/icons/instagram-icon.png"),
                  size: 20,
                ),
                SizedBox(width: 10.0),
                ImageIcon(
                  AssetImage("assets/icons/twitter-icon.png"),
                  size: 20,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
