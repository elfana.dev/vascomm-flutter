import 'package:flutter/material.dart';
import 'package:vascomm_flutter/theme/theme.dart';

class LayananCard extends StatelessWidget {
  final String imageUrl;

  LayananCard({this.imageUrl});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: edge),
      child: Container(
        width: 300.0,
        height: 149.0,
        decoration: BoxDecoration(
          image: DecorationImage(
            alignment: Alignment.centerRight,
            image: AssetImage(
              imageUrl,
            ),
          ),
          border: Border.all(
            color: Colors.white,
          ),
          color: Colors.white,
          borderRadius: BorderRadius.circular(16.0),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.3),
              spreadRadius: 2,
              blurRadius: 7,
              offset: Offset(0, 3),
            ),
          ],
        ),
        child: Row(
          children: <Widget>[
            SizedBox(width: 8),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    'PCR Swab Test (Drive Thru)\nHasil 1 Hari Kerja',
                    style: TextStyle(
                      fontFamily: 'Gilroy',
                      fontSize: 14,
                      fontWeight: FontWeight.w600,
                      color: mainColor,
                    ),
                  ),
                  SizedBox(height: 8),
                  Text(
                    'Rp. 1.400.000',
                    style: TextStyle(
                      fontFamily: 'Gilroy',
                      fontSize: 14,
                      fontWeight: FontWeight.w600,
                      color: Colors.orange,
                    ),
                  ),
                  SizedBox(height: 8),
                  Row(
                    children: [
                      ImageIcon(
                        AssetImage("assets/icons/building-icon.png"),
                        color: mainColor,
                        size: 14,
                      ),
                      SizedBox(width: 10),
                      Text('Lenmarc Surabaya'),
                    ],
                  ),
                  SizedBox(height: 8),
                  Row(
                    children: [
                      ImageIcon(
                        AssetImage("assets/icons/location-icon.png"),
                        color: mainColor,
                        size: 14,
                      ),
                      SizedBox(width: 10),
                      Text('Dukuh Pakis, Surabaya'),
                    ],
                  ),
                ],
              ),
            ),
            SizedBox(width: 70),
            /*    ClipRRect(
              borderRadius: BorderRadius.circular(16.0),
              child: Image.asset(
                imageUrl,
                width: 100.0,
                height: 135.0,
              ),
            ),*/
          ],
        ),
      ),
    );
  }
}
