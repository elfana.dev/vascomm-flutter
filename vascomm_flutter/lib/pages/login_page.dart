import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:vascomm_flutter/pages/home_page.dart';
import 'package:vascomm_flutter/pages/register_page.dart';
import 'package:vascomm_flutter/theme/theme.dart';

class LoginPage extends StatefulWidget {
  final bool isPassword = false;

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        bottom: false,
        child: Container(
          padding: EdgeInsets.all(5.0),
          height: MediaQuery.of(context).size.height,
          child: ListView(
            children: [
              SizedBox(height: 30.0),
              //NOTE: PAGE TITLE
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20.0),
                child: RichText(
                  text: TextSpan(
                    text: 'Hai, ',
                    style: TextStyle(
                      fontFamily: 'Gilroy',
                      fontSize: 28,
                      fontWeight: FontWeight.w700,
                      color: mainColor,
                    ),
                    children: <TextSpan>[
                      TextSpan(
                        text: 'Selamat Datang',
                        style: TextStyle(
                          fontFamily: 'GilroyBold',
                          fontSize: 28,
                          color: mainColor,
                        ),
                      )
                    ],
                  ),
                ),
              ),
              SizedBox(height: 4),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: edge),
                child: Text('Silahkan login untuk melanjutkan'),
              ),
              SizedBox(height: 25),
              Align(
                alignment: Alignment.centerRight,
                child: Container(
                  child: Image.asset(
                    "assets/images/login-asset-2.png",
                    width: 340.0,
                  ),
                ),
              ),
              SizedBox(height: 15),
              //NOTE: FORMS FIELD
              Container(
                padding: EdgeInsets.symmetric(horizontal: edge),
                child: Text(
                  'Email',
                  style: TextStyle(
                    fontFamily: 'Gilroy',
                    fontSize: 16,
                    fontWeight: FontWeight.w600,
                    color: mainColor,
                  ),
                ),
              ),
              SizedBox(height: 14),
              Center(
                child: Container(
                  decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey[300].withOpacity(0.3),
                        spreadRadius: 2,
                        blurRadius: 7,
                        offset: Offset(0, 3),
                      ),
                    ],
                  ),
                  width: 335.0,
                  margin: EdgeInsets.symmetric(horizontal: edge),
                  padding: EdgeInsets.symmetric(horizontal: 10.0),
                  child: TextField(
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      hintText: 'Masukkan email anda',
                    ),
                  ),
                ),
              ),
              SizedBox(height: 40),
              Row(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: edge),
                    child: Text(
                      'Password',
                      style: TextStyle(
                        fontFamily: 'Gilroy',
                        fontSize: 16,
                        fontWeight: FontWeight.w600,
                        color: mainColor,
                      ),
                    ),
                  ),
                  Spacer(),
                  Container(
                    padding: EdgeInsets.only(right: edge),
                    child: Text(
                      'Lupa Password anda ?',
                      style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w600,
                        color: mainColor,
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(height: 14),
              Center(
                child: Container(
                  width: 335.0,
                  margin: EdgeInsets.symmetric(horizontal: edge),
                  padding: EdgeInsets.symmetric(horizontal: 10.0),
                  decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey[300].withOpacity(0.3),
                        spreadRadius: 2,
                        blurRadius: 7,
                        offset: Offset(0, 3),
                      ),
                    ],
                  ),
                  child: TextField(
                    obscureText: true,
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      hintText: 'Masukkan password anda',
                      suffixIcon: Icon(
                        Icons.remove_red_eye,
                      ),
                    ),
                  ),
                ),
              ),
              Container(
                width: double.infinity,
                margin: EdgeInsets.only(top: 20.0),
                height: 48.0,
                padding: EdgeInsets.symmetric(horizontal: edge),
                child: RaisedButton(
                  color: mainColor,
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => HomePage(),
                      ),
                    );
                  },
                  elevation: 0,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8),
                  ),
                  child: Stack(
                    children: [
                      Center(
                        child: Text(
                          'Login',
                          style: TextStyle(
                            fontFamily: 'Gilroy',
                            fontSize: 16,
                            fontWeight: FontWeight.w600,
                            color: Colors.white,
                          ),
                        ),
                      ),
                      Align(
                        alignment: Alignment.centerRight,
                        child: Icon(
                          Icons.arrow_forward_rounded,
                          color: Colors.white,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(height: 20),
              Container(
                padding: EdgeInsets.symmetric(vertical: 10),
                alignment: Alignment.bottomCenter,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    RichText(
                      text: TextSpan(
                        text: 'Belum punya akun?',
                        style: TextStyle(
                          color: Colors.grey,
                          fontSize: 16,
                        ),
                        children: <TextSpan>[
                          TextSpan(
                            text: ' Daftar sekarang',
                            style: TextStyle(
                              color: mainColor,
                              fontSize: 16,
                              fontWeight: FontWeight.w500,
                            ),
                            recognizer: TapGestureRecognizer()
                              ..onTap = () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => RegisterPage(),
                                  ),
                                );
                              },
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 10),
              //NOTE: T&C
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  ImageIcon(
                    AssetImage("assets/icons/copyright-icon.png"),
                    color: Colors.grey,
                    size: 14,
                  ),
                  SizedBox(width: 10),
                  Text(
                    'SILK. all right reserved.',
                    style: TextStyle(
                      color: Colors.grey,
                    ),
                  ),
                ],
              ),
              SizedBox(height: 10),
            ],
          ),
        ),
      ),
    );
  }
}
