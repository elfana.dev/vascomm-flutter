import 'package:flutter/material.dart';
import 'package:vascomm_flutter/theme/theme.dart';
import 'package:vascomm_flutter/widgets/layanan_card.dart';
import 'package:vascomm_flutter/widgets/my_drawer.dart';
import 'package:vascomm_flutter/widgets/product_card.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  var _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(color: mainColor),
        actions: [
          //NOTE: ACTION BUTTONS
          ImageIcon(
            AssetImage("assets/icons/cart-icon.png"),
            color: mainColor,
            size: 20,
          ),
          SizedBox(width: 20),
          ImageIcon(
            AssetImage("assets/icons/reminder-icon.png"),
            color: mainColor,
            size: 20,
          ),
          SizedBox(width: 20),
        ],
        backgroundColor: Colors.white,
      ),
      drawer: MyDrawer(),
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      body: SafeArea(
        bottom: false,
        child: ListView(
          children: [
            SizedBox(height: 50),
            Container(
              padding: EdgeInsets.symmetric(horizontal: edge),
              child: Container(
                height: 161,
                width: 380,
                decoration: BoxDecoration(
                  border: Border.all(
                    color: Colors.white,
                  ),
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(16.0),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.2),
                      spreadRadius: 2,
                      blurRadius: 7,
                      offset: Offset(0, 3),
                    ),
                  ],
                  image: DecorationImage(
                    image: AssetImage('assets/images/cover-2.png'),
                    fit: BoxFit.fill,
                  ),
                ),
                child: Stack(
                  children: [
                    Align(
                      alignment: Alignment.topRight,
                      child: Image.asset("assets/images/solusi.png"),
                    ),
                    Padding(
                      padding: EdgeInsets.all(27.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          RichText(
                            text: TextSpan(
                              text: 'Solusi, ',
                              style: TextStyle(
                                fontFamily: 'Gilroy',
                                fontSize: 20,
                                fontWeight: FontWeight.w700,
                                color: mainColor,
                              ),
                              children: <TextSpan>[
                                TextSpan(
                                  text: 'Kesehatan Anda',
                                  style: TextStyle(
                                    fontFamily: 'GilroyBold',
                                    fontSize: 20,
                                    color: mainColor,
                                  ),
                                )
                              ],
                            ),
                          ),
                          SizedBox(height: 8),
                          Padding(
                            padding: EdgeInsets.only(right: 50.0),
                            child: Text(
                              'Update informasi seputar kesehatan semua bisa disini !',
                            ),
                          ),
                          SizedBox(height: 8),
                          Container(
                            height: 32.0,
                            width: 140,
                            child: RaisedButton(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8.0),
                              ),
                              color: mainColor,
                              onPressed: () {},
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                    'Selengkapnya',
                                    style: TextStyle(
                                      fontFamily: 'Gilroy',
                                      fontSize: 14,
                                      fontWeight: FontWeight.w600,
                                      color: Colors.white,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(height: 35),
            Container(
              padding: EdgeInsets.symmetric(horizontal: edge),
              child: Container(
                height: 141,
                width: 380,
                decoration: BoxDecoration(
                  border: Border.all(
                    color: Colors.white,
                  ),
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(16.0),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.2),
                      spreadRadius: 2,
                      blurRadius: 7,
                      offset: Offset(0, 3),
                    ),
                  ],
                ),
                child: Stack(
                  children: [
                    Align(
                      alignment: Alignment.topRight,
                      child: Image.asset("assets/images/vaccine.png"),
                    ),
                    Padding(
                      padding: EdgeInsets.all(27.0),
                      child: Wrap(
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                'Layanan Khusus',
                                style: TextStyle(
                                  fontFamily: 'Gilroy',
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              SizedBox(height: 8),
                              Padding(
                                padding: EdgeInsets.only(right: 55.0),
                                child: Text(
                                  'Tes Covid 19, Cegah Corona Sedini Mungkin',
                                ),
                              ),
                              SizedBox(height: 6),
                              Row(
                                children: [
                                  Text(
                                    'Daftar Tes',
                                    style: TextStyle(
                                      fontFamily: 'Gilroy',
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold,
                                      color: mainColor,
                                    ),
                                  ),
                                  Icon(
                                    Icons.arrow_forward_rounded,
                                    color: mainColor,
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(height: 35),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: edge),
              child: Container(
                height: 141,
                width: 380,
                decoration: BoxDecoration(
                  border: Border.all(
                    color: Colors.white,
                  ),
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(16.0),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.2),
                      spreadRadius: 2,
                      blurRadius: 7,
                      offset: Offset(0, 3),
                    ),
                  ],
                ),
                child: Row(
                  children: [
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: edge),
                      child: Image.asset(
                        "assets/images/track.png",
                        width: 100,
                      ),
                    ),
                    SizedBox(width: 20),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(height: 20),
                        Text(
                          'Track Pemeriksaan',
                          style: TextStyle(
                            fontFamily: 'Gilroy',
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                            color: mainColor,
                          ),
                        ),
                        SizedBox(height: 8),
                        Text(
                          'Kamu dapat mengecek \nprogress pemeriksaanmu disini',
                        ),
                        SizedBox(height: 12),
                        Row(
                          children: [
                            Text(
                              'Track',
                              style: TextStyle(
                                fontFamily: 'Gilroy',
                                fontSize: 18,
                                fontWeight: FontWeight.bold,
                                color: mainColor,
                              ),
                            ),
                            Icon(
                              Icons.arrow_forward_rounded,
                              color: mainColor,
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(height: 40),
            //NOTE: SEARCH FIELD
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 30),
                  child: ImageIcon(
                    AssetImage("assets/icons/filter-icon.png"),
                    color: mainColor,
                  ),
                ),
                Container(
                  width: 300.0,
                  margin: EdgeInsets.only(right: 20),
                  padding: EdgeInsets.symmetric(horizontal: 20.0),
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: Colors.white,
                    ),
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(30.0),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.2),
                        spreadRadius: 2,
                        blurRadius: 7,
                        offset: Offset(0, 3),
                      ),
                    ],
                  ),
                  child: TextField(
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      hintText: 'Search',
                      suffixIcon: Icon(
                        Icons.search_sharp,
                      ),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(height: 40),
            Container(
              height: 30.0,
              margin: EdgeInsets.symmetric(vertical: 20.0),
              child: ListView(
                scrollDirection: Axis.horizontal,
                children: <Widget>[
                  SizedBox(width: 20),
                  Container(
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: Colors.white,
                      ),
                      color: mainColor,
                      borderRadius: BorderRadius.circular(30.0),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.2),
                          spreadRadius: 2,
                          blurRadius: 7,
                          offset: Offset(0, 3),
                        ),
                      ],
                    ),
                    alignment: Alignment.center,
                    width: 111.0,
                    child: Text(
                      'All Product',
                      style: TextStyle(
                        fontFamily: 'Gilroy',
                        fontSize: 14,
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  SizedBox(width: 20),
                  Container(
                    alignment: Alignment.center,
                    width: 140.0,
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: Colors.white,
                      ),
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(30.0),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.2),
                          spreadRadius: 2,
                          blurRadius: 7,
                          offset: Offset(0, 3),
                        ),
                      ],
                    ),
                    child: Text(
                      'Layanan Kesehatan',
                      style: TextStyle(
                        fontFamily: 'Gilroy',
                        fontSize: 14,
                        fontWeight: FontWeight.bold,
                        color: mainColor,
                      ),
                    ),
                  ),
                  SizedBox(width: 20),
                  Container(
                    alignment: Alignment.center,
                    width: 140.0,
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: Colors.white,
                      ),
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(30.0),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.2),
                          spreadRadius: 2,
                          blurRadius: 7,
                          offset: Offset(0, 3),
                        ),
                      ],
                    ),
                    child: Text(
                      'Alat Kesehatan',
                      style: TextStyle(
                        fontFamily: 'Gilroy',
                        fontSize: 14,
                        fontWeight: FontWeight.bold,
                        color: mainColor,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: 26),
            Row(
              children: <Widget>[
                SizedBox(
                  width: 25,
                ),
                ProductCard(),
                SizedBox(width: 25),
                ProductCard(),
              ],
            ),
            SizedBox(height: 40),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: edge),
              child: Text(
                'Pilih Tipe Layanan Kesehatan Anda',
                style: TextStyle(
                  fontFamily: 'Gilroy',
                  fontSize: 16,
                  fontWeight: FontWeight.w600,
                  color: mainColor,
                ),
              ),
            ),
            SizedBox(
              height: 30,
            ),
            Row(
              children: <Widget>[
                SizedBox(width: 25),
                Container(
                  width: 78,
                  height: 28,
                  decoration: BoxDecoration(
                    color: Color(0XFF00D9D5),
                    borderRadius: BorderRadius.circular(20.0),
                  ),
                  child: Center(
                    child: Text(
                      'Satuan',
                      style: TextStyle(
                        fontFamily: 'Gilroy',
                        fontSize: 14,
                        fontWeight: FontWeight.w600,
                        color: mainColor,
                      ),
                    ),
                  ),
                ),
                SizedBox(width: 10),
                Container(
                  child: Text(
                    'Paket Pemeriksaan',
                    style: TextStyle(
                      fontFamily: 'Gilroy',
                      fontSize: 14,
                      fontWeight: FontWeight.w600,
                      color: mainColor,
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(height: 30),
            //NOTE: SERVICES / LAYANAN FIELD
            LayananCard(
              imageUrl: 'assets/images/lenmarc.png',
            ),
            SizedBox(height: 30),
            LayananCard(
              imageUrl: 'assets/images/lenmarc-2.png',
            ),
            SizedBox(height: 30),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 100),
              child: Row(
                children: <Widget>[
                  ImageIcon(
                    AssetImage("assets/icons/loading-icon.png"),
                    color: Colors.grey,
                    size: 16,
                  ),
                  SizedBox(
                    width: 8,
                  ),
                  Text('Tampilkan Lebih Banyak'),
                ],
              ),
            ),
            SizedBox(height: 20),
            //NOTE: BOTTOM BANNER
            Row(
              children: [
                Expanded(
                  child: InkWell(
                    onTap: () {},
                    child: Container(
                      child: Image.asset(
                        'assets/images/bottom-banner-2.png',
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
